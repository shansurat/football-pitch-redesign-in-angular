export interface Player {
    name: string,
    points: number,
    img: string
}

export interface Players {
    goalkeeper: Player,
    defenders: Player[]
    midfielders: Player[],
    forwards: Player[]
}

