import { Component, HostListener, OnInit } from '@angular/core';
import { Players } from '../player'

@Component({
  selector: 'app-lineup',
  templateUrl: './lineup.component.html',
  styleUrls: ['./lineup.component.scss']
})

export class LineupComponent implements OnInit {

  players: Players;
  scale: any;

  constructor() {
  }

  ngOnInit(): void {

    // Here is the players list :D
    // It is configured to have one and only one 
    // player as goalkeeper an the rest will have 
    // minimum of 3 players and maximum of 5
    
    this.players = {
      goalkeeper: { name: 'Alission', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_7_1-66.png" },
      defenders: [
        { name: 'Ramos', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_14-66.png" },
        { name: 'Ramos', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_14-66.png" },
        { name: 'Ramos', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_14-66.png" },
        { name: 'Ramos', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_14-66.png" }
      ],
      midfielders: [
        { name: 'Iniesta', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_7_1-66.png" },
        { name: 'Iniesta', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_7_1-66.png" },
        { name: 'Iniesta', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_7_1-66.png" },
        { name: 'Iniesta', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_7_1-66.png" },
        { name: 'Iniesta', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_7_1-66.png" }
      ],

      forwards: [
        { name: 'Ronaldo', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_14-66.png" },
        { name: 'Ronaldo', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_14-66.png" },
        { name: 'Ronaldo', points: 17, img: "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_14-66.png" }
      ]
    }


    this.setScale()

  }

  @HostListener('window:resize', ['$event'])
  setScale(): void {
    let scale = Math.min(
      window.innerWidth / 832,
      window.innerHeight / 623
    ) * 0.8
    this.scale = `scale(${scale})`
  }
}
